// startjvm.cpp : Defines the entry point for the console application.
//
// https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/invocation.html
//

#include "stdafx.h"

#include <jni.h>
#include <iostream>
#include <string>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	JavaVMOption jvmopt[1];
//	jvmopt[0].optionString = "-Djava.class.path=" + ".";
	jvmopt[0].optionString = "-Djava.class.path=.";

	JavaVMInitArgs vmArgs;
	vmArgs.version = JNI_VERSION_1_8;
	vmArgs.nOptions = 1;
	vmArgs.options = jvmopt;
	vmArgs.ignoreUnrecognized = JNI_TRUE;

	// Create the JVM
	JavaVM *javaVM;
	JNIEnv *jniEnv;
	long flag = JNI_CreateJavaVM(&javaVM, (void**)
		&jniEnv, &vmArgs);
	if (flag == JNI_ERR) {
		cout << "Error creating VM. Exiting...\n";
		return 1;
	}

	jclass jcls = jniEnv->FindClass("org/jnijvm/Demo");
	if (jcls == NULL) {
		jniEnv->ExceptionDescribe();
		javaVM->DestroyJavaVM();
		return 1;
	}
	if (jcls != NULL) {
		jmethodID methodId = jniEnv->GetStaticMethodID(jcls,
			"greet", "(Ljava/lang/String;)V");
		if (methodId != NULL) {
			jstring str = jniEnv->NewStringUTF("10");
			jniEnv->CallStaticVoidMethod(jcls, methodId, str);
			if (jniEnv->ExceptionCheck()) {
				jniEnv->ExceptionDescribe();
				jniEnv->ExceptionClear();
			}
		}
	}

	javaVM->DestroyJavaVM();
	return 0;
}


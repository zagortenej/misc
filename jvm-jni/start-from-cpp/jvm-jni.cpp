// https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/invocation.html

#include <jni.h>
#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;
/*
int staticallyLinked()
{
	JavaVMOption jvmopt[1];
//	jvmopt[0].optionString = "-Djava.class.path=" + ".";
	jvmopt[0].optionString = "-Djava.class.path=.";

	JavaVMInitArgs vmArgs;
	vmArgs.version = JNI_VERSION_1_8;
	vmArgs.nOptions = 1;
	vmArgs.options = jvmopt;
	vmArgs.ignoreUnrecognized = JNI_TRUE;

	// Create the JVM
	JavaVM *javaVM;
	JNIEnv *jniEnv;
	long flag = JNI_CreateJavaVM(&javaVM, (void**)
		&jniEnv, &vmArgs);
	if (flag == JNI_ERR) {
		cout << "Error creating VM. Exiting...\n";
		return 1;
	}

	jclass jcls = jniEnv->FindClass("org/jnijvm/Demo");
	if (jcls == NULL) {
		jniEnv->ExceptionDescribe();
		javaVM->DestroyJavaVM();
		return 1;
	}
	if (jcls != NULL) {
		jmethodID methodId = jniEnv->GetStaticMethodID(jcls,
			"greet", "(Ljava/lang/String;)V");
		if (methodId != NULL) {
			jstring str = jniEnv->NewStringUTF("10");
			jniEnv->CallStaticVoidMethod(jcls, methodId, str);
			if (jniEnv->ExceptionCheck()) {
				jniEnv->ExceptionDescribe();
				jniEnv->ExceptionClear();
			}
		}
	}

	javaVM->DestroyJavaVM();
	return 0;
}
*/
/**************************************************************
 *
 **************************************************************/
///////////////////////////////////////
// LINUX
///////////////////////////////////////
#include <dlfcn.h>
/* Load the specified shared library
 */
void * loadLibrary( const char * library ){
	void * result= dlopen(library, RTLD_LAZY);
	if(result == 0)
		printf("%s\n",dlerror());
	return result;
}

/* Unload the shared library
 */
void unloadLibrary( void * handle ){
	dlclose(handle);
}

/* Find the given symbol in the shared library
 */
void * findSymbol( void * handle, const char * symbol ){
	return dlsym(handle, symbol);
}
///////////////////////////////////////
// WINDOWS
///////////////////////////////////////

/**************************************************************
 *
 **************************************************************/
typedef jint (JNICALL *JNI_createJavaVM)(JavaVM **pvm, JNIEnv **env, void *args);

int main()
{
	void * jniLibrary;

	JavaVMOption jvmopt[1];
//	jvmopt[0].optionString = "-Djava.class.path=" + ".";
	// should not contain wildcards,
	// only list of folders which contain classes or
	// paths to jars
	jvmopt[0].optionString = (char*)"-Djava.class.path=./classes";
//	jvmopt[0].optionString = (char*)"-Djava.library.path=.";
	// windows: https://msdn.microsoft.com/en-us/library/ms686206.aspx
//	cout << "CLASSPATH 1 : " << getenv("CLASSPATH") << endl;
//	putenv((char*)"CLASSPATH=./classes");
//	cout << "CLASSPATH 2 : " << getenv("CLASSPATH") << endl;
//return 0;
	JavaVMInitArgs vmArgs;
	vmArgs.version = JNI_VERSION_1_8;
	vmArgs.nOptions = 1;
	vmArgs.options = jvmopt;
	vmArgs.ignoreUnrecognized = JNI_TRUE;

	// Create the JVM
	JavaVM *javaVM;
	JNIEnv *jniEnv;
	const char *libPath = "/usr/local/jdk1.8.0_60/jre/lib/amd64/server/libjvm.so";
	jniLibrary = loadLibrary(libPath);
	if(jniLibrary == NULL) {
		cout << "Error loading library " << libPath << endl;
		return 1;
	}

	JNI_createJavaVM createJavaVM;
	createJavaVM = (JNI_createJavaVM)findSymbol(jniLibrary, "JNI_CreateJavaVM");
	if(createJavaVM == NULL) {
		cout << "Couldn't find symbol 'JNI_CreateJavaVM' in " << libPath << endl;
		return 2;
	}


	long flag = createJavaVM(&javaVM, &jniEnv, &vmArgs);
	if (flag == JNI_ERR) {
		cout << "Error creating VM. Exiting...\n";
		return 1;
	}

	jclass jcls = jniEnv->FindClass("org/jnijvm/Demo");
	if (jcls == NULL) {
		jniEnv->ExceptionDescribe();
		javaVM->DestroyJavaVM();
		return 1;
	}
	if (jcls != NULL) {
		jmethodID methodId = jniEnv->GetStaticMethodID(jcls,
			"greet", "(Ljava/lang/String;)V");
		if (methodId != NULL) {
			jstring str = jniEnv->NewStringUTF("10");
			jniEnv->CallStaticVoidMethod(jcls, methodId, str);
			if (jniEnv->ExceptionCheck()) {
				jniEnv->ExceptionDescribe();
				jniEnv->ExceptionClear();
			}
		}
	}

	javaVM->DestroyJavaVM();
	return 0;
}

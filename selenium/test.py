#!/usr/bin/env python

"""
http://selenium-python.readthedocs.io/installation.html
https://seleniumhq.github.io/selenium/docs/api/py/api.html


http://screenster.io/running-tests-from-selenium-ide-in-chrome/

"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time



# browser = webdriver.Chrome()
# browser = webdriver.Firefox()

# special treatment for the IE drama-queen
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
caps = DesiredCapabilities.INTERNETEXPLORER
caps['ignoreProtectedModeSettings'] = True
browser = webdriver.Ie(capabilities=caps)

# browser = webdriver.Opera()
# for this you need to start server first
# browser = webdriver.Remote()
browser.implicitly_wait( 10 ) # seconds



# DEV
# browser.get('http://wpg1dwas001:9088/PteBackdoorPhase2/Login.faces?_ns=1')

# QAG
browser.get('http://wpg1qwas001:9089/PteBackdoorPhase2/Login.faces?_ns=1')



assert 'Backdoor Login for Policy Transaction Engine Testing' in browser.title

elem = browser.find_element_by_id('form1:txtUserID')  # Find the search box
elem.send_keys('locdev')

link_MB_hab_plus = browser.find_element_by_xpath("/html/body/form[1]/table/tbody/tr[2]/td[4]/a[3]")
link_MB_hab_plus.click()

time.sleep( 4 )

login = browser.find_element_by_id('form1:btnLogin')

login.click()




from selenium.webdriver.support.ui import Select

time.sleep( 2 )

assert 'Policy Transaction Engine Search' in browser.page_source

criteria_dropdown = browser.find_element_by_id('form1:somSearchType')
select = Select(criteria_dropdown)
select.select_by_visible_text('Client ID')

details = browser.find_element_by_id('form1:searchValue')
details.send_keys('BIE35')

time.sleep( 5 )

search_button = browser.find_element_by_id('form1:cmdSearch')
search_button.click()

time.sleep( 5 )

results_link_2 = browser.find_element_by_id('form1:dtSearchResults:1:lnkSelectClient')
results_link_2.click()

time.sleep( 5 )

browser.quit()


quit(0)


import contextlib
import selenium.webdriver as webdriver
import selenium.webdriver.support.ui as ui

with contextlib.closing(webdriver.Chrome()) as driver:
    driver.get('http://www.google.com')
    wait = ui.WebDriverWait(driver,10)
    # Do not call `implicitly_wait` if using `WebDriverWait`.
    #     It magnifies the timeout.
    # driver.implicitly_wait(10)  
    inputElement=driver.find_element_by_name('q')
    inputElement.send_keys('Cheese!')
    inputElement.submit()
    print(driver.title)

    wait.until(lambda driver: driver.title.lower().startswith('cheese!'))
    print(driver.title)

    # This raises
    #     selenium.common.exceptions.TimeoutException: Message: None
    #     after 10 seconds
    wait.until(lambda driver: driver.find_element_by_id('someId'))
    print(driver.title)

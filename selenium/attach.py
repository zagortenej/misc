#!/usr/bin/env python

"""
http://selenium-python.readthedocs.io/installation.html
https://seleniumhq.github.io/selenium/docs/api/py/api.html


http://screenster.io/running-tests-from-selenium-ide-in-chrome/

"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time


# special treatment for the IE drama-queen
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
caps = DesiredCapabilities.INTERNETEXPLORER
caps['ignoreProtectedModeSettings'] = True

browser = webdriver.Remote(command_executor='http://127.0.0.1:65317',desired_capabilities=caps)
browser.session_id = 'fc368953-1e57-451b-ae32-69e8e45a6ffa'

browser.implicitly_wait( 10 ) # seconds


elem = browser.find_element_by_id('form1:txtUserID')  # Find the search box
elem.send_keys('locdev')



# DEV
# browser.get('http://wpg1dwas001:9088/PteBackdoorPhase2/Login.faces?_ns=1')

# QAG
# browser.get('http://wpg1qwas001:9089/PteBackdoorPhase2/Login.faces?_ns=1')

time.sleep(10)

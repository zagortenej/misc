#!/usr/bin/env python

"""
http://selenium-python.readthedocs.io/installation.html
https://seleniumhq.github.io/selenium/docs/api/py/api.html


http://screenster.io/running-tests-from-selenium-ide-in-chrome/

"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time



# browser = webdriver.Chrome()
# browser = webdriver.Firefox()

# special treatment for the IE drama-queen
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
caps = DesiredCapabilities.INTERNETEXPLORER
caps['ignoreProtectedModeSettings'] = True
browser = webdriver.Ie(capabilities=caps)

# browser = webdriver.Opera()
# for this you need to start server first
# browser = webdriver.Remote()


url = browser.command_executor._url       #"http://127.0.0.1:60622/hub"
session_id = browser.session_id            #'4e167f26-dc1d-4f51-a207-f761eaf73c31'

print "browser = webdriver.Remote(command_executor='"+url+"',desired_capabilities={})"
print "browser.session_id = '"+session_id+"'"

browser.get('http://wpg1qwas001:9089/PteBackdoorPhase2/Login.faces?_ns=1')


while True:
    # once window is closed this will throw exception
    # and the script will end which is what we want
    handle = browser.current_window_handle
    time.sleep(2)


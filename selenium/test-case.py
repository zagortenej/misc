#!/usr/bin/env python

"""
http://selenium-python.readthedocs.io/installation.html
https://seleniumhq.github.io/selenium/docs/api/py/api.html


http://screenster.io/running-tests-from-selenium-ide-in-chrome/

"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time


from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert



driver = webdriver.Chrome()
# driver = webdriver.Firefox()
driver.set_window_position(0, 0)
driver.set_window_size(1280, 960)

# special treatment for the IE drama-queen
# from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# caps = DesiredCapabilities.INTERNETEXPLORER
# caps['ignoreProtectedModeSettings'] = True
# driver = webdriver.Ie(capabilities=caps)

# driver = webdriver.Opera()
# for this you need to start server first
# driver = webdriver.Remote()
driver.implicitly_wait( 10 ) # seconds





#############################################
# Helpers
#

def click_element_by_id( id ):
    el = driver.find_element_by_id(id)
    el.click()
    time.sleep( 1 )

def click_element_by_xpath( xpath ):
    el = driver.find_element_by_xpath(xpath)
    el.click()
    time.sleep( 1 )

def select_ddl_by_id( id, text ):
    ddl = driver.find_element_by_id( id )
    select = Select(ddl)
    select.select_by_visible_text( text )
    time.sleep( 1 )

def fill_text_by_id( id, text ):
    elem = driver.find_element_by_id( id )
    elem.send_keys( text )
    time.sleep( 1 )

def select_date_picker( id, day ):
    picker = driver.find_element_by_xpath('//input[contains(@id, "'+id+'")]/../span/i')
    picker.click()
    date = driver.find_element_by_xpath('//td[@data-event="click" and @data-handler="selectDay"]/a[contains(@class,"ui-state-default") and text()="'+day+'"]')
    date.click()
    time.sleep( 1 )

def wait_for_element_with_text( locator, content ):
    element = WebDriverWait(driver, 30).until(
        EC.text_to_be_present_in_element(locator, content)
    )

def wait_for_element( locator ):
    element = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located(locator)
    )


def wait_for_browser_to_close():
    while True:
        time.sleep(5)
        try:
            driver.find_element_by_tag_name("body")
        except:
            quit()


#
# Helpers - END
#############################################




# localhost
driver.get('http://localhost:8080/PteBackdoor/')

# STG
# driver.get('http://wpg1dwas001:9088/PteBackdoorStagingPhase2/Login.faces?_ns=1')

# QAG
# driver.get('http://wpg1qwas001:9089/PteBackdoorPhase2/Login.faces?_ns=1')


#############################################
# Login
#

username = 'autodev'

assert 'Backdoor Login for Policy Transaction Engine Testing' in driver.title

fill_text_by_id( 'form1:txtUserID', username )
# click_element_by_xpath( '/html/body/form[1]/table/tbody/tr[2]/td[4]/a[3]' )
click_element_by_xpath( '/html/body/form[1]/table/tbody/tr[2]/td[4]/a[5]' )
click_element_by_id( 'form1:btnLogin' )

#
# Login - END
#############################################


# Staging opens new window after login, QAG doesn't
# so we need to switch to new window if needed
# actually, this only happens for IE (sigh)

# print driver.current_window_handle
# print driver.window_handles


#############################################
# Find client
#

clientID = 'BIE518-test'

select_ddl_by_id( 'form1:somSearchType', 'Client ID' )
fill_text_by_id( 'form1:searchValue', clientID )
click_element_by_id( 'form1:cmdSearch' )
click_element_by_id( 'form1:dtSearchResults:0:lnkSelectClient' )

#
# Find client - END
#############################################


#############################################
# Create new Homeowner policy
#

click_element_by_id( 'form1:btnNewPolicy' )

select_ddl_by_id( 'form1:typeOfPolicy', 'Homeowner' )

select_date_picker( 'ApplicationEffectDate', '5' )

select_date_picker( 'textContInsuredSince', '4' )

select_ddl_by_id( 'form1:insuranceCompany', 'ABC Insurance' )

fill_text_by_id( 'form1:previousPolicyNumber', '123456' )

click_element_by_id( 'form1:cmdOK' )

#
# Create new policy - END
#############################################


#############################################
# Edit Primary Location
#
click_element_by_id( 'ui-id-2' )

click_element_by_id( 'form1:cmdEditPrimaryLocation' )

#
# Fill in the form
#
### REQUIRED FIELDS - START
#
click_element_by_id( 'form1:loc__LegalAddress__copyAddressFromClient' )

click_element_by_id( 'form1:LocFactor_FindRatingTerritory' )

fill_text_by_id( 'form1:loc__GRC', '1000000' )

fill_text_by_id( 'form1:loc__Coverage', '1000000' )

fill_text_by_id( 'form1:loc__PercentOfGRC', '100' )

fill_text_by_id( 'form1:loc__YearBuilt', '1999' )

select_ddl_by_id( 'form1:loc__EvaluatorType', 'Applied ITV' )

select_date_picker( 'loc__EvaluatorDate', '1' )

select_ddl_by_id( 'form1:loc__HotTub', 'None' )

select_ddl_by_id( 'form1:loc__SwimmingPool', 'None' )

fill_text_by_id( 'form1:loc__Construction_BrickSolid', '100' )

#
# Add heating item
#
click_element_by_xpath( '//input[@value="Add Heating Item"]' )

click_element_by_xpath( '//input[@value="FURNACES/BOILERS"]' )

select_ddl_by_id( 'form1:heatingSystem', 'Central' )

select_ddl_by_id( 'form1:fuel', 'N/Gas' )
select_ddl_by_id( 'form1:chimney', 'Direct Vent' )
select_ddl_by_id( 'form1:heatingUse', 'Primary' )
select_ddl_by_id( 'form1:heatingDistribution', 'Forced Air' )


click_element_by_id( 'form1:cmdOk' )
#
# Add heating item DONE
#
#
### REQUIRED FIELDS - END

#
# Optional Coverages
#
CDA_amount = '10000'
fill_text_by_id( 'form1:loc__AdditionalCoverage_00171_Coverage', CDA_amount )


# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC

# no need to wait for primary location form to show up
# because OK button ID's are different: 'cmdOk' vs 'cmdOK' (geez!)
# wait_for_element_with_text( (By.TAG_NAME, "h1"), 'Primary Location (Home)' )

click_element_by_id( 'form1:cmdOK' )


#
# Edit Primary Location - END
#############################################

# time.sleep(30)


#############################################
# Convert location
#

click_element_by_id( 'form1:cmdConvertPrimaryLocation' )
select_ddl_by_id( 'form1:somPrimaryLocationTypes', 'Condominium' )
click_element_by_id( 'form1:cmdNext' )

Alert(driver).accept()

wait_for_element_with_text( (By.TAG_NAME, "h1"), 'Primary Location (Condominium)' )

driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
driver.save_screenshot('CDA.png')

# confirm that CDA still contains a value

input = driver.find_element_by_id( 'form1:loc__AdditionalCoverage_00171_Coverage' )
CDAvalue = input.get_attribute('value').encode('utf-8')
assert CDAvalue == CDA_amount

click_element_by_id( 'form1:cmdOK' )

time.sleep(10)


driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
driver.save_screenshot('converted.png')

#
# Convert location - END
#############################################

time.sleep(30)

#############################################
# Step_name
#

#
# Step_name - END
#############################################


driver.quit()

quit(0)


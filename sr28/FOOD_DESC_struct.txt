CREATE TABLE `food_desc` (

`NDB_No` varchar(5) NOT NULL COMMENT '5-digit Nutrient Databank number that uniquely identifies a food item. If this field is defined as numeric, the leading zero will be lost.',
`FdGrp_Cd` varchar(4) NOT NULL COMMENT '4-digit code indicating food group to which a food item belongs.',
`Long_Desc` varchar(200) NOT NULL COMMENT '200-character description of food item.',
`Shrt_Desc` varchar(60) NOT NULL COMMENT '60-character abbreviated description of food item. Generated from the 200-character description using abbreviations in Appendix A. If short description is longer than 60 characters, additional abbreviations are made.',
`ComName` varchar(100) NULL COMMENT 'Other names commonly used to describe a food, including local or regional names for various foods, for example, “soda” or “pop” for “carbonated beverages.”',
`ManufacName` varchar(65) NULL COMMENT 'Indicates the company that manufactured the product, when appropriate.',
`Survey` varchar(1) NULL COMMENT 'Indicates if the food item is used in the USDA Food and Nutrient Database for Dietary Studies (FNDDS) and thus has` varchar(complete nutrient profile for the 65 FNDDS nutrients.',
`Ref_desc` varchar(135) NULL COMMENT 'Description of inedible parts of a food item (refuse), such as seeds or bone.',
`Refuse` decimal(2) NULL COMMENT 'Percentage of refuse.',
`SciName` varchar(65) NULL COMMENT 'Scientific name of the food item. Given for the least processed form of the food (usually raw), if applicable.',
`N_Factor` decimal(4,2) NULL COMMENT 'Factor for converting nitrogen to protein.',
`Pro_Factor` decimal(4,2) NULL COMMENT 'Factor for calculating calories from protein.',
`Fat_Factor` decimal(4,2) NULL COMMENT 'Factor for calculating calories from fat.',
`CHO_Factor` decimal(4,2) NULL COMMENT 'Factor for calculating calories from carbohydrate.'

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `food_desc` ADD PRIMARY KEY (`NDB_No`);

ALTER TABLE `food_desc` ADD FULLTEXT `shrt_desc_fulltext_idx` (`Shrt_Desc`(60));
ALTER TABLE `food_desc` ADD FULLTEXT `long_desc_fulltext_idx` (`Long_Desc`(60));



LOAD DATA LOCAL INFILE '/home/boxer/misc/sr28/FOOD_DES_ascii.txt' INTO TABLE `food_desc` FIELDS TERMINATED BY '^' OPTIONALLY ENCLOSED BY '~';


SELECT NDB_No, Shrt_Desc, Long_Desc FROM `food_desc` WHERE MATCH(`Long_Desc`) AGAINST ('chicken breast cooked' IN NATURAL LANGUAGE MODE);

SELECT NDB_No, Shrt_Desc, Long_Desc FROM `food_desc` WHERE MATCH(`Long_Desc`) AGAINST ('+chicken +breast +cooked' IN BOOLEAN MODE);


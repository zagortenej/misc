based on
https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-14-04


---------------------------------------------------------------------------------------------------

== install and configure openvpn

sudo apt-get install -y openvpn easy-rsa

gunzip -c /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz | sudo tee /etc/openvpn/server.conf > /dev/null

nano /etc/openvpn/server.conf

dh dh1024.pem
to
dh dh2048.pem

;push "redirect-gateway def1 bypass-dhcp"
to 
push "redirect-gateway def1 bypass-dhcp"

;push "dhcp-option DNS 208.67.222.222"
;push "dhcp-option DNS 208.67.220.220"
to 
push "dhcp-option DNS 208.67.222.222"
push "dhcp-option DNS 208.67.220.220"

;user nobody
;group nogroup
to
user nobody
group nogroup


== enable packet forwarding

enable packet forwarding in kernel

echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward > /dev/null

to make it permanent

sudo nano /etc/sysctl.conf

#net.ipv4.ip_forward=1
to
net.ipv4.ip_forward=1


enable nat masquerading

sudo modprobe iptable_nat
sudo iptables -t nat -A POSTROUTING -s 10.8.0.1/2 -o eth0 -j MASQUERADE



== setup firewall

In AWS add rule

port: 1194
protocol: UDP
source: your-ip-address


== configure and build the CA

sudo cp -r /usr/share/easy-rsa/ /etc/openvpn

sudo mkdir /etc/openvpn/easy-rsa/keys

sudo nano /etc/openvpn/easy-rsa/vars

change these
export KEY_COUNTRY="US"
export KEY_PROVINCE="TX"
export KEY_CITY="Dallas"
export KEY_ORG="My Company Name"
export KEY_EMAIL="sammy@example.com"
export KEY_OU="MYOrganizationalUnit"

if you want different name for server (and key/crt files), change:

export KEY_NAME="server"

server.key and server.crt will be named after this, and you need to update references in openvpn config files.

generate DH parameters

sudo openssl dhparam -out /etc/openvpn/dh2048.pem 2048

=== build the CA

cd /etc/openvpn/easy-rsa

sudo su

. ./vars
./clean-all
./build-ca

=== generate certificate and key for server

Still in /etc/openvpn/easy-rsa and with 'sudo su':

./build-key-server server

where 'server' is same as KEY_SERVER in /etc/openvpn/easy-rsa/vars (if you changed it)

when asked for challenge password and confirmation, just press enter.

Answer 'y' when asked to sign the cert and to commit operation.

exit 'sudo su'

exit



=== restart openvpn with new keys

sudo cp /etc/openvpn/easy-rsa/keys/{server.crt,server.key,ca.crt} /etc/openvpn

ll /etc/openvpn

sudo service openvpn restart
sudo service openvpn status

check /var/log/syslog for errors, if any.

(i.e. Options error: --key fails with 'server.key': No such file or directory)

tail -n 30 /var/log/syslog | grep ovpn-server


== generate crt and key for client

cd /etc/openvpn/easy-rsa

sudo su

. ./vars

build 'client1' key. Name the client as you wish and use different names for different clients

./build-key client1

do the same drill as for when we did server cert/key, answering questions, not using challenge password,
and comitting changes at the end.

ll keys/client1.*

should show

keys/client1.crt
keys/client1.csr
keys/client1.key


get the sample client.conf file

sudo cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn/easy-rsa/keys/client.ovpn

clients usually expect config file with ovpn extension, so we changed it.

exit 'sudo su'

exit

=== files needed for the client to connect

these files have to be the same ones on all clients:

/etc/openvpn/easy-rsa/keys/client.ovpn
/etc/openvpn/ca.crt

so copy those.

Every client will have its own cert and key, so for 'client1' copy:

/etc/openvpn/easy-rsa/keys/client1.crt
/etc/openvpn/easy-rsa/keys/client1.key

at the end, client should end up with these files:


client1.crt
client1.key
client.ovpn
ca.crt

pack the files for transport:

sudo tar cvfj client.tar.bz2 -C /etc/openvpn/easy-rsa/keys client.ovpn client1.crt client1.key -C /etc/openvpn ca.crt

copy client.tar.bz2 to your client.


== set up openvpn on client worstation (Linux)


unpack the client.tar.bz2

tar xvfj client.tar.bz2


rename client.ovpn file to something sensible, the name is what openvpn client will show in status:

mv client.ovpn name-of-connection.ovpn

edit name-of-connection.ovpn file

remote my-server-1 1194
change 'my-server-1' to server's ip address or domain (if it has one)

;user nobody
;group nogroup
to
user nobody
group nogroup



---------------------------------------------------------------------------------------------------



